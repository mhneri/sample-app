FROM node:10-alpine
RUN apk update

WORKDIR /app
ENV APPLICANT_NAME="CHANGE THIS NAME"
ENV HOST_IP=192.168.10.214

COPY package.json package.json
RUN npm install

COPY views views
COPY index.js index.js
COPY app.sh app.sh

EXPOSE 8080
ENTRYPOINT [ "/bin/sh" ]
CMD ["./app.sh"]