const express = require('express');
const axios = require('axios');

const app = express();

app.set('view engine', 'pug');

app.use('/', (req, res) => {

  const hostIP = process.env.HOST_IP || '127.0.0.1';

  axios.get(`http://${hostIP}:5984/images/wallpaper`)
    .then((response) => {
      const name = process.env.APPLICANT_NAME || 'The Quick Brown Fox'
      res.render('index', {name, img: response.data.data})
    });
});

app.listen(8080, () => {
  console.log('listening at 8080')
});